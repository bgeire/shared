#!/usr/bin/env ruby

require 'unleash'
require 'unleash/context'

# please review https://gitlab.com/bgeire/shared/-/blob/main/use_flags.mov to learn how to retrieve the URL
unleash = Unleash::Client.new({
  url: 'YOUR_URL/api/v4/feature_flags/unleash/FEATURE_FLAG_ID',
  app_name: 'ENVIRONMENT_NAME', 
  instance_id: 'INSTANCE_ID'
})

# user_id is hard coded here for brevity. That shoukld be taken from the user profile, environment variable or an external parameter.
unleash_context = Unleash::Context.new
unleash_context.user_id = "A_UNIQUE_USER_IDENTIFIER_SUCH_AS_GUID" 

if unleash.is_enabled?("FLAG_NAME", unleash_context)
  puts "The feature flag is enabled for all users accessing the production instance."
else
  puts "You can deploy your application’s new features to production in smaller batches. Feature flags reduce risk, allowing you to do controlled testing, and separate feature delivery."
end
